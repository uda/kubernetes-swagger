# Kubernetes Swagger API Explorer

Explore the APIs for Kubernetes in the Swagger-UI.

All `swagger.json` files are linked to https://github.com/kubernetes/kubernetes/tree/master/api/openapi-spec, each one is the respective git tag's version.

## Usage

Go to [https://uda.gitlab.io/kubernetes-swagger/](https://uda.gitlab.io/kubernetes-swagger/), select the version you want to explore, depending on your browser, the rendering of elements might take some time.

To view a specific version, use the definition selector, or replace `{VERSION}` in the URL `https://uda.gitlab.io/kubernetes-swagger/?urls.primaryName=Kubernetes API {VERSION}`.

## Copyright

Kubernetes and Swagger are each distributed under the Apache 2.0 license, this repository is for educational purposes only.

The following custom content is published as well with the terms of the Apache 2.0 license:

* `build.py`
* `README.md`
* `public/schema-list.json`
* `public/index.html` (modified from the swagger-ui version)

You can find more information at the following repositories:

* https://github.com/kubernetes/kubernetes
* https://github.com/swagger-api/swagger-ui
