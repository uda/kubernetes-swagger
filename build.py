#!/usr/bin/env python3

import json
import os
from pathlib import Path

import requests
from semantic_version import Version


def fetch_releases():
    github_auth = None
    if 'GITHUB_AUTH' in os.environ:
        github_auth = tuple(os.environ['GITHUB_AUTH'].split(':', 1))

    page = 1
    page_size = 50
    initial_response = requests.get(
        url='https://api.github.com/repos/kubernetes/kubernetes/releases',
        params={'page': page, 'per_page': page_size},
        headers={'Accept': 'application/vnd.github.v3+json'},
        auth=github_auth,
    )
    data = initial_response.json()
    received_size = len(data)
    while True:
        for release in data:
            yield release
        if received_size != page_size:
            break
        page += 1
        response = requests.get(
            url='https://api.github.com/repos/kubernetes/kubernetes/releases',
            params={'page': page, 'per_page': page_size},
            headers={'Accept': 'application/vnd.github.v3+json'},
            auth=github_auth,
        )
        data = response.json()
        received_size = len(data)
    return


def main():
    base_url = 'https://raw.githubusercontent.com/kubernetes/kubernetes/{tag_name}/api/openapi-spec/swagger.json'
    minimum_version = Version('1.5.0-alpha.2')

    schema_list_file = Path('public/schema-list.json')
    schema_list = {}
    if schema_list_file.exists():
        raw_schema_list = json.loads(schema_list_file.read_text())
        # We assume the schema list file already has only SemVer valid versions
        schema_list = {Version(s['tag_name'][1:]): s for s in raw_schema_list}

    for release in fetch_releases():
        if 'tag_name' not in release:
            continue
        tag_name = release['tag_name']
        name = f'Kubernetes API {tag_name}'
        # schema_filename = f'schemas/k8s-swagger-{tag_name}.json'
        try:
            semver = Version(tag_name[1:])
        except ValueError:
            continue
        if semver < minimum_version:
            continue
        schema_list[semver] = {
            'name': name,
            'url': base_url.format(tag_name=tag_name),
            'tag_name': tag_name,
        }
        # schema_file = Path(schema_filename)
        # if schema_file.exists():
        #     continue
        # schema = requests.get(url=base_url.format(tag_name=tag_name))
        # schema_file.write_text(schema.text)

    schema_list_file.write_text(json.dumps([s for t, s in sorted(schema_list.items(), reverse=True)]))


if __name__ == '__main__':
    main()
